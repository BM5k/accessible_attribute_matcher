require 'accessible_attribute_matcher/version'
require 'rspec'
require 'rspec-expectations'

module AccessibleAttributeMatcher

  RSpec::Matchers.define :expose do |field|

    chain(:to) { |r| @role = r }

    # set the default role for accessible_attributes
    def role
      @role ||= :default
    end

    # simplify access to the described_class
    def described_class
      @described_class ||= actual.class
    end

    description do
      "expose #{ field.inspect } to #{ role.inspect }"
    end

    failure_message_for_should do
      "Expected #{ described_class } to expose #{ field.inspect } as #{ role.inspect }"
    end

    failure_message_for_should_not do
      "Expected #{ described_class } not to expose #{ field.inspect } as #{ role.inspect }"
    end

    match do |actual|
      described_class.accessible_attributes(role).include? field.to_sym
    end

  end

end
