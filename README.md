AccessibleAttribute Matcher
===========================

[![build status][ci-image]][ci]
[![code climate][cc-image]][cc]
[![gem version][gem-image]][gem]

About
-----

Use Rails 3 accessible_attributes reflection to test attr_accessible / attr_protected.

URLs:

- Github:        https://github.com/bm5k/accessible_attribute_matcher/
- Documentation: http://rubydoc.info/github/BM5k/accessible_attribute_matcher/master/frames
- RubyGems:      https://rubygems.org/gems/accessible_attribute_matcher

Installation
------------

Add to your Gemfile and run the `bundle` command to install it.

  ```ruby
  gem 'accessible_attribute_matcher'
  ```

**Requires Ruby 1.9.2 or later.**

Usage
-----

  ```ruby
  class Foo < ActiveRecord::Base
    attr_accessible :field_a
    attr_accessible :field_b, as: [:admin]
    attr_protected  :field_c
  end

  require 'spec_helper'

  describe Foo do

    it { should     expose(:field_a) }
    it { should_not expose(:field_b).to(:default) }
    it { should     expose(:field_b).to(:admin) }
    it { should_not expose(:field_c) }

  end
  ```

<!-- links -->
[cc]: https://codeclimate.com/github/BM5k/accessible_attribute_matcher "code climate"
[ci]: http://travis-ci.org/BM5k/accessible_attribute_matcher "build status"
[gem]: http://badge.fury.io/rb/accessible_attribute_matcher "gem version"

<!-- images -->
[cc-image]: https://codeclimate.com/github/BM5k/accessible_attribute_matcher.svg
[ci-image]: https://secure.travis-ci.org/BM5k/accessible_attribute_matcher.svg?branch=master
[gem-image]:https://badge.fury.io/rb/accessible_attribute_matcher.svg
