require 'spec_helper'

describe Thing do

  it { should_not expose(:field_b).to(:default) }
  it { should_not expose(:field_c)              }
  it { should     expose(:field_a)              }
  it { should     expose(:field_b).to(:admin)   }

end
