%w[ accessible_attribute_matcher active_support/core_ext active_model ].each { |lib| require lib }

class Thing

  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::MassAssignmentSecurity

  attr_accessor :field_a, :field_b, :field_c

  attr_accessible :field_a
  attr_accessible :field_b, as: [:admin]
  attr_protected  :field_c

end
