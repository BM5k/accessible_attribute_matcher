# -*- encoding: utf-8 -*-
$:.push File.expand_path('../lib', __FILE__)
require 'accessible_attribute_matcher/version'

Gem::Specification.new do |s|
  s.name        = 'accessible_attribute_matcher'
  s.version     = AccessibleAttributeMatcher::VERSION
  s.authors     = ['BM5k']
  s.email       = ['me@bm5k.com']
  s.homepage    = ""
  s.summary     = 'RSpec matcher for ActiveModel accessible_attributes'
  s.description = 'Use reflection to spec ActiveModel accessible_attributes'
  s.licenses    = %['MIT']

  s.rubyforge_project = 'accessible_attribute_matcher'

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ['lib']

  s.required_ruby_version = ">= 1.9.3"

  s.add_dependency 'activemodel',   '~> 3.2'
  s.add_dependency 'activesupport', '~> 3.2'
  s.add_dependency 'rspec',         '~> 2'
end
